# Projeto de PEI

### **Objetivo:** criar um lista de smartphones da Worten e da FNAC, bem como registar o histórico de preços de cada produto.

### **Software Utilizado:**
- <a href="#"><img src="https://www.pngkit.com/png/detail/206-2063294_the-postman-logo-is-available-in-png-svg.png" width="150px" alt="Postman"></a>
- <a href="#"><img src="http://micheee.github.io/images/static/basex-web-slides/webroot/images/BaseX.png" width="85px" alt="BaseX" /></a>
- **IDE**
    - <a href="#"><img src="https://i0.wp.com/macossoftware.net/wp-content/uploads/2018/03/oXygen-XML-Editor-2018-Crack-For-Mac-2.jpg?resize=612%2C262&ssl=1" width="150px" alt="Oxygen" /></a>
    - <a href="#"><img src="https://blog.launchdarkly.com/wp-content/uploads/2018/10/visualstudio_code-card.png" width="200px" alt="Visual Studio Code" /></a>
        - **Plugins recomendados:**
            - XML Tools
            - XML Language Support
            - XML Format
            - XML Complete